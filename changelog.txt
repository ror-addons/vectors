
== Changelog ==

=== Version 1.00.4: ===
* fixed rare crash of ActionBarClusterManager (Mythic) that was triggered by loading order issues and foreign anchors
* removed extra chat window support as it is now supplied by Mythic

=== Version 1.00.3: ===
* fixed the remaining Mythics LayoutEditor functions that could be called from outside

=== Version 1.00.2: ===
* fixed Mythics unfriendly LayoutEditor functions

=== Version 1.00.1: ===
* fixed settings window corruption on live resolution change

=== Version 1.00: ===
* fixed loading order problems with a lot of addons
* added dynamic window loading on dependency window creation
* added automatic chat window registration
* added mouse over window quick select
* fixed resolution scale for some resolutions
* made settings window bigger
* fixed squared scale calculation
* fixed ratio miscalculation on some resolutions
* fixed several settings window bugs

=== Version Beta1: ===
* initial public upload
